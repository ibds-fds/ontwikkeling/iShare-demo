

Documentation is quite fragmented.
To get to understand something like delegation, it takes quite some reading in both the functional and technical documentation.
A lot of sequence diagrams leave out details without mentioning it.
Often there seem to be multiple ways to do things (like delegation), without giving a lot of details about the different ways.
Different documentation seem to give sometimes conflicting information


Support on slack is fast


The postman examples are tricky to get to work, documentation is spread over the [support](https://support.ishare.eu/support/solutions/articles/101000513092-postman-collection) and [developer](https://dev.ishare.eu/demo-and-testing/postman.html) sites, but they are very useful to get a better technical understanding of how it actually works together.


It doesn't seem to be possible/desired to have a completely stand-alone trust framework, ie. be your own "Scheme Owner" (for testing or otherwise), so even for testing there are hard to automate steps to get going (eg. requesting certificates).


There are 10 (!) different *templates* of docker-compose files, rather than a single one, just to run the satellite, making things very complex to run.


There's a `prerequsites.sh` script for the satellite that uses sudo/root on the host, and only works on Ubuntu (even tho it's claimed that Debian should work). This feels like an unnecessary safety risk.


Reference Implementation of the Authorization Registry is hopelessly outdated. It uses .Net core 3.1, which has reached end of life and being unsupported since December 2022. It does not build with more updated versions of .Net.
