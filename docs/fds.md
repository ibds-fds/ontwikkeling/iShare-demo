# Federatief Datastelsel (FDS)

## Introductie

In dit document zal worden gekeken naar meerdere producten die de rol van Federatied Dataselsel voor de overheid op zich zouden kunnen nemen.
Dit zou in de vorm van een standaard moeten zijn.

## Producten in de markt

### iShare

#### Werking

IShare heeft een hub en spoke model.
Per dataspace is er een satellite, die in verbinding staat met een parent satellite.
De satellite is de plek waar alle deelnemers staan geregistreerd.
Machtigen/Delegeren gaat door middel van policies.
Een Entitled Party kan daarin aangeven welke service rechten hij delegeert aan een specifieke service consumer.
Deze policies kunnen in een authorization registry worden vastgelegd.

#### Conclusie

Op zich is iShare een prima product om data binnen een dataspace te delen.
Dit zijn echter de problemen waardoor het minder goed zou werken als de FDS oplossing.
- Het maakt wel gebruik van standaarden, maar is zelf geen standaard.
- IShare lijkt ownership te willen houden doordat alle satellites in verbinding moeten staan met de root satellite.
- Doordat het geen standaard is en de documentatie en bestaande APIs niet compleet zijn, is implementatie en aansluiting niet makkelijk zonder support. 
- De beschikbare APIs/Software op github zijn vaak outdated

### FSC

#### Werking

Standaardiseerd de koppeling tussen organisaties door voor te schrijven hoe het managementverkeer en het dataverkeer moet verlopen tussen API's van organisaties. 
Het is een peer-to-peer netwerk, waarbij het gebruik van het centrale component, de "directory", optioneel is. 
Ook biedt FSC een veilige manier voor organisaties om andere organisaties te delegeren, data op te halen bij een bron en/of kunnen organisaties API's namens een andere organisatie aanbieden.
Daarnaast biedt FSC ook standaarden aan voor audit en technische logging.

#### Conclusie

FSC heeft hier het voordeel, doordat het zichzelf als een standaard neerzet. 
Daarnaast is er een referentie-implementatie die geheel open source is.
De referentie-implementatie heeft ook een zeer uitgebreide installatie handleiding, waardoor het eenvoudiger is om zelfstandig aan de slag te gaan 


### TSG

#### Werking

#### Conclusie