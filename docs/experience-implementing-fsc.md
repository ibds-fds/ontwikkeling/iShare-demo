Very nice documentation.

The critique are more some minor issues.

There is documentation on how to deploy the application using docker-compose in the README of the gitlab project.
And there is documentation on the [website](https://docs.fsc.nlx.io/try-fsc/helm/3-postgresql) on how to deploy on both docker-compose and kubernetes with helm.
Maybe one place would be better.

Some things don't work on my machine.
There are some small mistakes, especially with naming (nlx and fsc prefixes)