# IShare participants

Voor de technische werking [iShare Developer Portal](https://dev.ishare.eu/)
Voor het functionele plaatje [iShare Trust Framework](https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70222191/iSHARE+Trust+Framework)

## Service Provider
Party that provides data to be consumed by Service Consumer.

## Service Consumer
Party that consumes data provided by a Service Provider.
Consumer can be either system or human.
Either the Service Consumer is the entitled party for the data provided by a Service Provider.
Or the Service Consumer has delegated rights from an entitled party for the data provided by a Service Provider.

## IShare Satellite / Satellite Administrator
Fundamental role as a trusted 3rd party
Onboarding and validating participants.
Keeps track of all participants in the data space using a ledger system. 
Empowers data owners to control access and usage rights for their data.
[iShare Satellite explained](https://ishare.eu/ishare-satellite-explained/)

## Authorisation Registry
Contains Delegation and authorisation information

## Identity Provider / Identity Broker
Authentication for humans

## Technische werking
- [Swagger / OpenAPI](https://w13.isharetest.net/swagger)
- [iShare Github](https://github.com/iSHAREScheme)
- [Docker images](https://hub.docker.com/search?q=isharefoundation)

## JWT

### authentication
IShare is leaning a lot on JWT (JSON Web Tokens).
Authentication is done by making a JWT token with your own PKI private key 
This token is sent to the party whose restricted data you want to access (at `/access/token` endpoint)
An access token is sent back
This access token needs to be used when accessing the restricted endpoints.

### iShare response
(Almost) all of the iShare endpoints return the result as JWT token wrapped in JSON. 

## Design thoughts
Service providers and consumers communicate directly to each other.
It's the service providers own responsibility to check with the iShare satellite for authentication and authorization.
[iShare authentication flow](https://dev.ishare.eu/reference/authentication.html#machine-to-machine-m2m-authentication)
[iShare authorization](https://dev.ishare.eu/reference/authorization.html#machine-to-machine-m2m-authorization)

Service consumers can check with the iShare satellite the list of trusted service providers
