# Certificates and Key Material

## Usage of JWT tokens

iShare makes use extensive use of JWT tokens for authentication.

The client's authentication is verified through the signature on the JWT token.
An OAuth Access token or an OpenID Connect ID token is returned.

Delegation evidence is a signed JWT. 
The proof is in the Authorization registry's or Entitled Party's signature.

Metadata response is sent by a server as a signed JWT.

JSON Web Encryption (JWE) may be used.

Identification of parties is done with EORI identifiers (e.g. EU.EORI.NL123456789).

More info at the [iShare JWT documentation](https://dev.ishare.eu/reference/jwt.html).

## Key material

iShare makes use of PKI keys and certificates.

[Getting a test key/certificate](https://dev.ishare.eu/demo-and-testing/test-certificates.html).
It's best to get an offline private key (in p12 format). 
This key can easily be converted to the necessary certificates and public keys in PEM format.

```shell
openssl pkcs12 -in <keyname>.p12 -out <keyname>.key.pem -nocerts -nodes -legacy -passin pass:<password>
openssl pkcs12 -in <keyname>.p12 -out <keyname>.crt.pem -nokeys -nodes -legacy -passin pass:<password> 
```

## Python library
[documentation](https://pypi.org/project/python-ishare/)



